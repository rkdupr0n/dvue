package org.siemens.dvue.departmentms.api;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppEmployee;
import org.siemens.dvue.dbservice.domain.AppOrganization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {

    @Autowired
    private RestTemplate template;

    @Value("${department.baseUrl}")
    private String baseUrl;

    // $ Adding new Department
    @PostMapping(value = "/add")
    public ResponseEntity<AppDepartment> addDepartment(@RequestBody AppDepartment department) {
        URI uri = URI.create(baseUrl + "/add");
        return ResponseEntity.created(uri).body(template.postForObject(uri, department, AppDepartment.class));
    }

    // $ Getting all respective Departments
    @GetMapping(value = "/all")
    @SuppressWarnings(value = "unchecked")
    public ResponseEntity<Map<String, Object>> getAllDepartments(@RequestHeader(defaultValue = "0") String page,
    @RequestHeader(defaultValue = "5") String size) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("page", page);
        headers.add("size", size);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/all").headers(headers).build();
        Map<String, Object> res = template.exchange(req, Map.class).getBody();
        return ResponseEntity.ok(res);
    }

    // $ Getting a specific instance of a Department
    @GetMapping(value = "/get")
    public ResponseEntity<AppDepartment> getDepartment(@RequestHeader String depName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get").headers(headers).build();
        return template.exchange(req, AppDepartment.class);
    }

    // $ Department specific
    @GetMapping(value = "/get/assets")
    public ResponseEntity<List<AppAsset>> getDepartmentAssets(@RequestHeader String depName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get/assets").headers(headers).build();
        return ResponseEntity.ok(List.of(template.exchange(req, AppAsset[].class).getBody()));
    }

    @GetMapping(value = "/get/employees")
    public ResponseEntity<List<AppEmployee>> getDepartmentEmployees(@RequestHeader String depName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get/employees").headers(headers).build();
        return ResponseEntity.ok(List.of(template.exchange(req, AppEmployee[].class).getBody()));
    }

    @GetMapping(value = "/get/organization")
    public ResponseEntity<AppOrganization> getDepartmentOrganization(@RequestHeader String depName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get/organization").headers(headers).build();
        return template.exchange(req, AppOrganization.class);
    }

    // $ Binding entities
    @PostMapping(value = "/bind/asset")
    public ResponseEntity<Boolean> bindAssetToDepartment(@RequestHeader String sno, @RequestHeader String depName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("sno", sno);
        headers.add("depName", depName);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/bind/asset").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    @PostMapping(value = "/bind/employee")
    public ResponseEntity<Boolean> bindEmployeeToDepartment(@RequestHeader String empName,
            @RequestHeader String depName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        headers.add("depName", depName);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/bind/employee").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Updating Department
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateDepartment(@RequestHeader String depName,
            @RequestBody AppDepartment department) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        RequestEntity<AppDepartment> req = RequestEntity.post(baseUrl + "/update").headers(headers).body(department);
        return template.exchange(req, Boolean.class);
    }

    // $ Deleting Department
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteDepartment(@RequestHeader String depName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        RequestEntity<Void> req = RequestEntity.delete(baseUrl + "/delete").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Unbinding entities
    @PostMapping(value = "/unbind/asset")
    public ResponseEntity<Boolean> unbindAssetFromDepartment(@RequestHeader String depName, @RequestHeader String sno) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        headers.add("sno", sno);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/unbind/asset").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    @PostMapping(value = "/unbind/employee")
    public ResponseEntity<Boolean> unbindEmployeeFromDepartment(@RequestHeader String empName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/unbind/employee").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }
}
