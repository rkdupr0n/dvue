package dvue.lib.cast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CastList {
    public static <T> List<T> castList(Class<? extends T> clazz, Collection<?> rawCollection)
            throws ClassCastException {
        List<T> result = new ArrayList<>(rawCollection.size());
        for (Object o : rawCollection) {
            result.add(clazz.cast(o));
        }
        return result;
    }
}
