import { createStore } from "vuex";

export default createStore({
  state: {
    user: {
      access_token: "",
      refresh_token: "",
      error: undefined,
    },
  },
  mutations: {
    setTokens(state, userTokens) {
      if (userTokens && userTokens.access_token && userTokens.refresh_token) {
        state.user.access_token = userTokens.access_token;
        state.user.refresh_token = userTokens.refresh_token;
      } else {
        state.user.error = userTokens;
      }
    },

    clearTokens(state) {
      state.user.access_token = "";
      state.user.refresh_token = "";
    },
  },
  actions: {},
  modules: {},
});
