export enum EntityName {
  ORGANIZATION = "organization",
  DEPARTMENT = "department",
  EMPLOYEE = "employee",
  ASSET = "asset",
  ASSETTYPE = "asset-type",
}

export enum EntityNameList {
  ORGANIZATION = "organizations",
  DEPARTMENT = "departments",
  EMPLOYEE = "employees",
  ASSET = "assets",
  ASSETTYPE = "assetTypes",
}

export enum SimplifiedEntityNameList {
  ORGANIZATION = "simplifiedOrganizations",
  DEPARTMENT = "simplifiedDepartments",
  EMPLOYEE = "simplifiedEmployees",
  ASSET = "simplifiedAssets",
  ASSETTYPE = "simplifiedAssetTypes",
}
