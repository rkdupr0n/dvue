export default {
  toUpperCamelCase(text: string) {
    return this.toCamelCase(text).replace(/^[a-z]/g, (x: string) =>
      x[0].toUpperCase()
    );
  },

  toCamelCase(text: string) {
    return text.replace(/-\w/g, this.clearAndUpper);
  },

  toPascalCase(text: string) {
    return text.replace(/(^\w|-\w)/g, this.clearAndUpper);
  },

  clearAndUpper(text: string) {
    return text.replace(/-/, "").toUpperCase();
  },
};
