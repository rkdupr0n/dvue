import AssetType from "./AssetType";

export default class Asset {
  constructor(
    public sno: string,
    public name: string,
    public startDate: string,
    public endDate: string,
    public assetType: AssetType
  ) {}
}
