export default class AssetType {
  constructor(
    public id: number,
    public type: string,
    public description: string
  ) {}
}
