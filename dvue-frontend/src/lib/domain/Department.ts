import Asset from "./Asset";
import Employee from "./Employee";

export default class Department {
  constructor(
    public id: number,
    public orgId: number,
    public depName: string,
    public assets: Asset[],
    public employees: Employee[]
  ) {}
}
