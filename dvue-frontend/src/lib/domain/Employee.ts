import Asset from "./Asset";

export default class Employee {
  constructor(
    public gid: number,
    public empName: string,
    public email: string,
    public phoneNo: string,
    public depId: number,
    public assets: Asset[]
  ) {}
}
