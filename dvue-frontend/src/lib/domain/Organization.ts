import Department from "./Department";

export default class Organization {
  constructor(
    public id: number,
    public orgName: string,
    public departments: Department[]
  ) {}
}
