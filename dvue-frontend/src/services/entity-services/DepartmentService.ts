import Asset from "@/lib/domain/Asset";
import Employee from "@/lib/domain/Employee";
import Department from "@/lib/domain/Department";
import Organization from "@/lib/domain/Organization";
import { EntityName } from "@/lib/enums/entityName";
import CES from "../CommonEntityService";

const baseName = EntityName.DEPARTMENT;
const entityIdName = "depName";

export default {
  async add(department: Department): Promise<Department> {
    return CES.add(baseName, department);
  },

  async all(): Promise<Department[]> {
    return CES.all(baseName);
  },

  async get(depName: string): Promise<Department> {
    return CES.get(baseName, entityIdName, depName);
  },

  async getAssets(depName: string): Promise<Asset[]> {
    return CES.getSpecific(
      baseName,
      entityIdName,
      depName,
      EntityName.ASSET,
      true
    );
  },

  async getEmployees(depName: string): Promise<Employee[]> {
    return CES.getSpecific(
      baseName,
      entityIdName,
      depName,
      EntityName.EMPLOYEE,
      true
    );
  },

  async getOrganization(depName: string): Promise<Organization> {
    return CES.getSpecific(
      baseName,
      entityIdName,
      depName,
      EntityName.ORGANIZATION,
      false
    );
  },

  async bindAsset(sno: string, depName: string): Promise<boolean> {
    return CES.bindEntity(
      baseName,
      EntityName.ASSET,
      "sno",
      entityIdName,
      sno,
      depName
    );
  },

  async bindEmployee(empName: string, depName: string): Promise<boolean> {
    return CES.bindEntity(
      baseName,
      EntityName.EMPLOYEE,
      "empName",
      entityIdName,
      empName,
      depName
    );
  },

  async update(depName: string, department: Department): Promise<boolean> {
    return CES.update(baseName, entityIdName, depName, department);
  },

  async delete(depName: string): Promise<boolean> {
    return CES.delete(baseName, entityIdName, depName);
  },

  async unbindAsset(depName: string, sno: string): Promise<boolean> {
    return CES.unbindEntity(
      baseName,
      EntityName.ASSET,
      "sno",
      entityIdName,
      sno,
      depName
    );
  },

  async unbindEmployee(empName: string): Promise<boolean> {
    return CES.unbindEntity(
      baseName,
      EntityName.EMPLOYEE,
      "empName",
      undefined,
      empName,
      undefined
    );
  },
};
