import Asset from "@/lib/domain/Asset";
import Employee from "@/lib/domain/Employee";
import Department from "@/lib/domain/Department";
import { EntityName } from "@/lib/enums/entityName";
import CES from "../CommonEntityService";

const baseName = EntityName.EMPLOYEE;
const entityIdName = "empName";

export default {
  async add(employee: Employee): Promise<Employee> {
    return CES.add(baseName, employee);
  },

  async all(): Promise<Employee[]> {
    return CES.all(baseName);
  },

  async get(empName: string): Promise<Employee> {
    return CES.get(baseName, entityIdName, empName);
  },

  async getAssets(empName: string): Promise<Asset[]> {
    return CES.getSpecific(
      baseName,
      entityIdName,
      empName,
      EntityName.ASSET,
      true
    );
  },

  async getDepartment(empName: string): Promise<Department> {
    return CES.getSpecific(
      baseName,
      entityIdName,
      empName,
      EntityName.DEPARTMENT,
      false
    );
  },

  async bindAsset(sno: string, empName: string): Promise<boolean> {
    return CES.bindEntity(
      baseName,
      EntityName.ASSET,
      "sno",
      entityIdName,
      sno,
      empName
    );
  },

  async update(empName: string, employee: Employee): Promise<boolean> {
    return CES.update(baseName, entityIdName, empName, employee);
  },

  async delete(empName: string): Promise<boolean> {
    return CES.delete(baseName, entityIdName, empName);
  },

  async unbindAsset(empName: string, sno: string): Promise<boolean> {
    return CES.unbindEntity(
      baseName,
      EntityName.ASSET,
      "sno",
      entityIdName,
      sno,
      empName
    );
  },
};
