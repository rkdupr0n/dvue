import AssetType from "@/lib/domain/AssetType";
import { EntityName } from "../../lib/enums/entityName";
import { Http } from "../../lib/enums/http";
import api from "../api";
import CES from "../CommonEntityService";

const baseName = EntityName.ASSETTYPE;
const entityIdName = "type";

export default {
  async add(assetType: AssetType): Promise<AssetType> {
    return CES.add(baseName, assetType);
  },

  async all(): Promise<AssetType[]> {
    return CES.all(baseName);
  },

  async get(type: string): Promise<AssetType> {
    return CES.get(baseName, entityIdName, type);
  },

  async desc(type: string): Promise<string> {
    return api(baseName + "/desc", {
      method: Http.GET,
      headers: {
        type,
      },
    });
  },

  async update(type: string, assetType: AssetType): Promise<boolean> {
    return CES.update(baseName, entityIdName, type, assetType);
  },

  async delete(type: string): Promise<boolean> {
    return CES.delete(baseName, entityIdName, type);
  },
};
