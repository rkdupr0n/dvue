import Asset from "@/lib/domain/Asset";
import { EntityName } from "@/lib/enums/entityName";
import { Http } from "../../lib/enums/http";
import api from "../api";
import CES from "../CommonEntityService";

const baseName = EntityName.ASSET;
const entityIdName = "sno";

export default {
  async add(asset: Asset): Promise<Asset> {
    return CES.add(baseName, asset);
  },

  async all(): Promise<Asset[]> {
    return CES.all(baseName);
  },

  async get(sno: string): Promise<Asset> {
    return CES.get(baseName, entityIdName, sno);
  },

  async checkValidity(sno: string, date: string): Promise<boolean> {
    return api(baseName + "/check-validity", {
      method: Http.GET,
      headers: {
        sno,
        date,
      },
    });
  },

  async bindAssetType(type: number, sno: string): Promise<boolean> {
    return CES.bindEntity(
      baseName,
      EntityName.ASSETTYPE,
      "type",
      entityIdName,
      type.toString(),
      sno
    );
  },

  async update(sno: string, asset: Asset): Promise<boolean> {
    return CES.update(baseName, entityIdName, sno, asset);
  },

  async delete(sno: string): Promise<boolean> {
    return CES.delete(baseName, entityIdName, sno);
  },

  async unbindAssetType(sno: string): Promise<boolean> {
    return CES.unbindEntity(
      baseName,
      EntityName.ASSETTYPE,
      undefined,
      entityIdName,
      undefined,
      sno
    );
  },
};
