import Department from "@/lib/domain/Department";
import Organization from "@/lib/domain/Organization";
import { EntityName } from "@/lib/enums/entityName";
import CES from "../CommonEntityService";

const baseName = EntityName.ORGANIZATION;
const entityIdName = "orgName";

export default {
  async add(organization: Organization): Promise<Organization> {
    return CES.add(baseName, organization);
  },

  async all(): Promise<Organization[]> {
    return CES.all(baseName);
  },

  async get(orgName: string): Promise<Organization> {
    return CES.get(baseName, entityIdName, orgName);
  },

  async getDepartments(orgName: string): Promise<Department[]> {
    return CES.getSpecific(
      baseName,
      entityIdName,
      orgName,
      EntityName.DEPARTMENT,
      true
    );
  },
  async bindDepartment(depName: string, orgName: string): Promise<boolean> {
    return CES.bindEntity(
      baseName,
      EntityName.DEPARTMENT,
      "depName",
      entityIdName,
      depName,
      orgName
    );
  },
  async update(orgName: string, organization: Organization): Promise<boolean> {
    return CES.update(baseName, entityIdName, orgName, organization);
  },
  async delete(orgName: string): Promise<boolean> {
    return CES.delete(baseName, entityIdName, orgName);
  },
  async unbindDepartment(depName: string): Promise<boolean> {
    return CES.unbindEntity(
      baseName,
      EntityName.DEPARTMENT,
      "depName",
      undefined,
      depName,
      undefined
    );
  },
};
