import api from "./api";
import { Http } from "../lib/enums/http";
import store from "../store";

export default {
  async login(username: string, password: string): Promise<any> {
    try {
      const userTokens = await api(
        "login",
        {
          method: Http.POST,
          body: new URLSearchParams({
            username,
            password,
          }),
        },
        false
      );
      store.commit("setTokens", userTokens);
      return userTokens;
    } catch (e) {
      return {
        message: "Authentication Failed",
        failure: true,
      };
    }
  },

  async refreshTokens(): Promise<any> {
    const refreshToken = store.state.user.refresh_token;
    if (refreshToken) {
      const userTokens = await api("token/refresh", {
        method: Http.GET,
        headers: {
          Authorization: `Bearer ${refreshToken}`,
        },
      });
      return userTokens;
    } else {
      return { error_message: "Refresh Tokens are unavailable" };
    }
  },

  async checkLoggedIn(): Promise<boolean> {
    const userTokens = await this.refreshTokens();
    return userTokens.access_token != null && userTokens.refresh_token != null;
  },

  logout() {
    store.commit("clearTokens");
  },
};
