import api from "@/services/api.js";

export default {
  register(credentials: string) {
    return api("/user/register", {
      method: "POST",
      body: JSON.stringify(credentials),
    });
  },
  login(credentials: string) {
    return api("/user/login", {
      method: "POST",
      body: JSON.stringify(credentials),
    });
  },
  user() {
    return api("/user");
  },
};
