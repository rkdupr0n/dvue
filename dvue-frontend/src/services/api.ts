import store from "../store";

export default async function api(
  url: string,
  params: { [headers: string]: {} } = {},
  isAuth = true
): Promise<any> {
  params = Object.assign(
    {
      // mode: "cors",
      // cache: "no-cache",
    },
    params
  );

  params.headers = Object.assign(
    isAuth
      ? {
          Authorization: `Bearer ${store.state.user.access_token}`,
          "Content-Type": "application/json",
        }
      : {},
    params.headers
  );

  const response = await fetch((isAuth ? "/api/" : "/") + url, params);
  const json = (await response.json()) || {};
  if (!response.ok) {
    const errorMessage = json.error
      ? json.error.error || json.error
      : response.status;
    throw new Error(errorMessage);
  }
  return json;
}
