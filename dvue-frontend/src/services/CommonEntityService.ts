import api from "./api";
import { Http } from "../lib/enums/http";

const headers: { [key: string]: string } = {};

export default {
  async add(baseName: string, entity: any): Promise<any> {
    return api(baseName + "/add", {
      method: Http.POST,
      body: JSON.stringify(entity),
    });
  },

  async all(baseName: string, page: number, size: number): Promise<any[]> {
    return api(baseName + "/all", {
      method: Http.GET,
      headers: {
        page,
        size,
      },
    });
  },

  async get(
    baseName: string,
    entityIdName: string,
    entityId: string
  ): Promise<any> {
    headers[entityIdName] = entityId;

    return api(baseName + "/get", {
      method: Http.GET,
      headers,
    });
  },

  async getSpecific(
    baseName: string,
    entityIdName: string,
    entityId: string,
    specificEntityName: string,
    isMultiple: boolean
  ): Promise<any> {
    headers[entityIdName] = entityId;

    return api(
      baseName + "/get/" + specificEntityName + isMultiple ? "s" : "",
      {
        method: Http.GET,
        headers,
      }
    );
  },

  async bindEntity(
    baseName: string,
    childBaseName: string,
    childIdName: string,
    entityIdName: string,
    childId: string,
    entityId: string
  ): Promise<boolean> {
    headers[childIdName] = childId;
    headers[entityIdName] = entityId;

    return api(baseName + "/bind/" + childBaseName, {
      method: Http.POST,
      headers,
    });
  },

  async unbindEntity(
    baseName: string,
    childBaseName: string,
    childIdName?: string,
    entityIdName?: string,
    childId?: string,
    entityId?: string
  ): Promise<boolean> {
    if (childId && childIdName) headers[childIdName] = childId;
    if (entityId && entityIdName) headers[entityIdName] = entityId;

    return api(baseName + "/unbind/" + childBaseName, {
      method: Http.POST,
      headers,
    });
  },

  async update(
    baseName: string,
    entityIdName: string,
    entityId: string,
    entity: any
  ): Promise<boolean> {
    headers[entityIdName] = entityId;

    // To prevent conflicts in backend and is unnecessary data
    delete entity[entityIdName];

    return api(baseName + "/update", {
      method: Http.POST,
      headers,
      body: JSON.stringify(entity),
    });
  },

  async delete(
    baseName: string,
    entityIdName: string,
    entityId: string
  ): Promise<boolean> {
    headers[entityIdName] = entityId;

    return api(baseName + "/delete", {
      method: Http.DELETE,
      headers,
    });
  },
};
