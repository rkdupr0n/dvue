package org.siemens.dvue.userauth.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.siemens.dvue.userauth.domain.AppRole;
import org.siemens.dvue.userauth.domain.AppUser;
import org.siemens.dvue.userauth.repo.AppRoleRepo;
import org.siemens.dvue.userauth.repo.AppUserRepo;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
// import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Transactional
// @Slf4j
public class AppUserServiceImpl implements AppUserService, UserDetailsService {
    private final AppUserRepo userRepo;
    private final AppRoleRepo roleRepo;
    private final PasswordEncoder passwordEncoder;

    @Override
    public AppUser saveAppUser(AppUser user) {
        // log.info("Saving new user {} to our database", user.getName());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepo.save(user);
    }

    @Override
    public AppRole saveAppRole(AppRole role) {
        // log.info("Saving new role {} to our database", role.getRole());
        return roleRepo.save(role);
    }

    @Override
    public void addAppRoleToAppUser(String username, String roleName) {
        // log.info("Adding role {} to user {}", roleName, username);
        AppUser user = userRepo.findByUsername(username);
        AppRole role = roleRepo.findByRole(roleName);
        user.getRoles().add(role);
    }

    @Override
    public AppUser getUser(String username) {
        // log.info("Getting user {}", username);
        return userRepo.findByUsername(username);
    }

    @Override
    public List<AppUser> getUsers() {
        // log.info("Getting all users");
        return userRepo.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user = userRepo.findByUsername(username);
        if (user == null) {
            // log.error("User not found in the database");
            throw new UsernameNotFoundException("User not found in the database");
        } else {
            // log.info("User {} found in the database", username);
        }

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getRole()));
        });
        return new User(username, user.getPassword(), authorities);
    }
}
