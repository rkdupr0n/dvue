package org.siemens.dvue.userauth;

import java.util.ArrayList;

import org.siemens.dvue.userauth.domain.AppRole;
import org.siemens.dvue.userauth.domain.AppUser;
import org.siemens.dvue.userauth.service.AppUserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class DvueUserauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(DvueUserauthApplication.class, args);
	}
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner run(AppUserService userService) {
		return args -> {
			// $ Adding new roles
			userService.saveAppRole(new AppRole(null, "ROLE_USER"));
			userService.saveAppRole(new AppRole(null, "ROLE_MANAGER"));
			userService.saveAppRole(new AppRole(null, "ROLE_ADMIN"));
			userService.saveAppRole(new AppRole(null, "ROLE_SUPER_ADMIN"));

			// $ Adding new users
			userService.saveAppUser(new AppUser(null, "Bob Stuart", "bobby", "qwerty", new ArrayList<>()));
			userService.saveAppUser(new AppUser(null, "Phil Nye", "philnye", "qwerty", new ArrayList<>()));
			userService.saveAppUser(new AppUser(null, "Kelly Ranger", "kellran", "qwerty", new ArrayList<>()));
			userService.saveAppUser(new AppUser(null, "Tom Broody", "tommy", "qwerty", new ArrayList<>()));

			// $ Adding roles to users
			userService.addAppRoleToAppUser("bobby", "ROLE_USER");
			userService.addAppRoleToAppUser("tommy", "ROLE_USER");
			userService.addAppRoleToAppUser("philnye", "ROLE_USER");
			userService.addAppRoleToAppUser("kellran", "ROLE_USER");
			userService.addAppRoleToAppUser("tommy", "ROLE_SUPER_ADMIN");
			userService.addAppRoleToAppUser("philnye", "ROLE_ADMIN");
			userService.addAppRoleToAppUser("tommy", "ROLE_MANAGER");
			userService.addAppRoleToAppUser("kellran", "ROLE_MANAGER");
		};
	}
}
