package org.siemens.dvue.userauth.repo;

import org.siemens.dvue.userauth.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepo extends JpaRepository<AppUser, Long> {
	AppUser findByUsername(String username);
}
