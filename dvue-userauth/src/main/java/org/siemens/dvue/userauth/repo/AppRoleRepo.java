package org.siemens.dvue.userauth.repo;

import org.siemens.dvue.userauth.domain.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRoleRepo extends JpaRepository<AppRole, Long> {
    AppRole findByRole(String role);
}
