package org.siemens.dvue.userauth.service;

import java.util.List;

import org.siemens.dvue.userauth.domain.AppRole;
import org.siemens.dvue.userauth.domain.AppUser;

public interface AppUserService {
    AppUser saveAppUser(AppUser user);

    AppRole saveAppRole(AppRole role);

    void addAppRoleToAppUser(String username, String roleName);

    AppUser getUser(String username);

    List<AppUser> getUsers();
}
