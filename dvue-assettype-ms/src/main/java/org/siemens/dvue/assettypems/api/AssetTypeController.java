package org.siemens.dvue.assettypems.api;

import java.net.URI;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppAssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/asset-type")
public class AssetTypeController {

    @Autowired
    private RestTemplate template;

    @Value("${assetType.baseUrl}")
    private String baseUrl;

    // $ Adding new AssetType
    @PostMapping(value = "/add")
    public ResponseEntity<AppAssetType> addAssetType(@RequestBody AppAssetType assetType) {
        URI uri = URI.create(baseUrl + "/add");
        return ResponseEntity.created(uri)
                .body(template.postForObject(baseUrl + "/add", assetType, AppAssetType.class));
    }

    // $ Getting all respective AssetTypes
    @GetMapping(value = "/all")
    @SuppressWarnings(value = "unchecked")
    public ResponseEntity<Map<String, Object>> getAllAssetTypes(@RequestHeader(defaultValue = "0") String page,
            @RequestHeader(defaultValue = "5") String size) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("page", page);
        headers.add("size", size);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/all").headers(headers).build();
        Map<String, Object> res = template.exchange(req, Map.class).getBody();
        return ResponseEntity.ok(res);
    }

    // $ Getting a specific instance of an AssetType
    @GetMapping(value = "/get")
    public ResponseEntity<AppAssetType> getAssetType(@RequestHeader String type) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("type", type);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get").headers(headers).build();
        return template.exchange(req, AppAssetType.class);
    }

    // $ AssetType specific
    @GetMapping(value = "/desc")
    public ResponseEntity<String> getAssetTypeDescription(@RequestHeader String type) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("type", type);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/desc").headers(headers).build();
        return template.exchange(req, String.class);
    }

    // $ Updating AssetType
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateAssetType(@RequestHeader String type, @RequestBody AppAssetType assetType) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("type", type);
        RequestEntity<AppAssetType> req = RequestEntity.post(baseUrl + "/update").headers(headers).body(assetType);
        return template.exchange(req, Boolean.class);
    }

    // $ Deleting AssetType
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteAssetType(@RequestHeader String type) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("type", type);
        RequestEntity<Void> req = RequestEntity.delete(baseUrl + "/delete").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }
}
