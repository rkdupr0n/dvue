package org.siemens.dvue.assettypems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan(basePackages = {"org.siemens.dvue.assettypems", "org.siemens.dvue.dbservice.domain"})
public class DvueAssettypeMsApplication {

	@Bean
	RestTemplate template() {
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(DvueAssettypeMsApplication.class, args);
	}

}
