package org.siemens.dvue.employeems.api;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private RestTemplate template;

    @Value("${employee.baseUrl}")
    private String baseUrl;

    // $ Adding new Employee
    @PostMapping(value = "/add")
    public ResponseEntity<AppEmployee> addEmployee(@RequestBody AppEmployee employee) {
        URI uri = URI.create(baseUrl + "/add");
        return ResponseEntity.created(uri).body(template.postForObject(uri, employee, AppEmployee.class));
    }

    // $ Getting all respective Employees
    @GetMapping(value = "/all")
    @SuppressWarnings(value = "unchecked")
    public ResponseEntity<Map<String, Object>> getAllEmployees(@RequestHeader(defaultValue = "0") String page,
    @RequestHeader(defaultValue = "5") String size) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("page", page);
        headers.add("size", size);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/all").headers(headers).build();
        Map<String, Object> res = template.exchange(req, Map.class).getBody();
        return ResponseEntity.ok(res);
    }

    // $ Getting a specific instance of an Employee
    @GetMapping(value = "/get")
    public ResponseEntity<AppEmployee> getEmployee(@RequestHeader String empName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get").headers(headers).build();
        return template.exchange(req, AppEmployee.class);
    }

    // $ Employee specific
    @GetMapping(value = "/get/assets")
    public ResponseEntity<List<AppAsset>> getEmployeeAssets(@RequestHeader String empName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get/assets").headers(headers).build();
        return ResponseEntity.ok(List.of(template.exchange(req, AppAsset[].class).getBody()));
    }

    @GetMapping(value = "/get/department")
    public ResponseEntity<AppDepartment> getEmployeeDepartment(@RequestHeader String empName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get/department").headers(headers).build();
        return template.exchange(req, AppDepartment.class);
    }

    // $ Binding entities
    @PostMapping(value = "/bind/asset")
    public ResponseEntity<Boolean> bindAssetToEmployee(@RequestHeader String sno, @RequestHeader String empName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        headers.add("sno", sno);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/bind/asset").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Updating Employee
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateEmployee(@RequestHeader String empName, @RequestBody AppEmployee employee) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        RequestEntity<AppEmployee> req = RequestEntity.post(baseUrl + "/update").headers(headers).body(employee);
        return template.exchange(req, Boolean.class);
    }

    // $ Deleting Employee
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteEmployee(@RequestHeader String empName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        RequestEntity<Void> req = RequestEntity.delete(baseUrl + "/delete").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Unbinding entities
    @PostMapping(value = "/unbind/asset")
    public ResponseEntity<Boolean> unbindAssetFromEmployee(@RequestHeader String empName, @RequestHeader String sno) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("empName", empName);
        headers.add("sno", sno);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/unbind/asset").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }
}
