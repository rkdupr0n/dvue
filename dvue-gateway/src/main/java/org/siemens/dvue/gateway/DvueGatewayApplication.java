package org.siemens.dvue.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DvueGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(DvueGatewayApplication.class, args);
	}

}
