#!/usr/bin/bash
sp=`awk '/dvue-/ {sp=substr($1,2,length($1)-3);if(sp!="dvue-lib")print sp}' ../../settings.gradle`
nsp=()
port=0
for i in $sp;
do
    port=`awk '/server.port=/ {print substr($1,13,length($1))}' ../../$i/src/main/resources/application.properties`;
    nsp+="$i\t$port\n";
done
echo -e $nsp > subProjects.gen.txt