#!/usr/bin/bash
mapfile -t sp < <(awk '{print $1}' subProjects.gen.txt)
mapfile -t pp < <(awk '{print $2}' subProjects.gen.txt)
sudo docker start dvue
for i in ${!sp[*]};
do
    if [[ "${sp[$i]}" == "" || "${pp[$i]}" == "" ]];
    then continue;
    else sudo docker run -p ${pp[$i]}:${pp[$i]} --name ${sp[$i]} --net dvue-net --env-file ../../configs/mariaenv -d --rm ${sp[$i]};
    sleep 3;
    fi
done
