from requests import post
from random import randint
from datetime import datetime, timedelta
from apiFetch import getFakeEmps, getFakeAssets


def postEmps(url, count, deps):
    emps = []
    fakeEmps = getFakeEmps(count)['data']

    for i in fakeEmps:
        # ^ Replacing the ' in first and last names
        name = i['name'].replace("'", '', 1) + ' ' + \
            i['surname'].replace("'", '', 1)
        emp = {
            'empName': name,
            'email': i['email'],
            'phoneNo': i['phone']
        }

        addEmpHttp = post(url + 'employee/add', json=emp)
        emps.append(addEmpHttp.json())

        bindDepEmpHttp = post(url + 'department/bind/employee', headers={
            'empName': name,
            'depName': deps[randint(0, len(deps) - 1)]['depName']
        })
    return emps


def postAssets(url, count, deps, emps, ats):
    assets = []
    fakeAssets = getFakeAssets(count)

    for i in fakeAssets:
        sd = datetime(randint(2021, 2030), 1, 1, randint(0, 23), randint(
            0, 59), randint(0, 59), randint(0, 999999)) + timedelta(randint(0, 364))
        ed = datetime(randint(2031, 2050), 1, 1, randint(0, 23), randint(
            0, 59), randint(0, 59), randint(0, 999999)) + timedelta(randint(0, 364))
        asset = {
            'sno': 'sno' + str(randint(0, 1000000000)),
            'name': i.replace('_', ' '),
            'startDate': sd.isoformat(timespec='milliseconds'),
            'endDate': ed.isoformat(timespec='milliseconds')
        }

        tries = 0
        while tries < 10:
            addAssetHttp = post(url + 'asset/add', json=asset)
            if not addAssetHttp.ok:
                asset['sno'] = str(randint(0, 1000000000))
                tries += 1
            else:
                assets.append(addAssetHttp.json())
                break

        at = 0
        if randint(0, 100) < 60:  # ^ 60% chance of binding asset to employee
            at = 1
            bindEmpAssetHttp = post(url + 'employee/bind/asset', headers={
                'sno': asset['sno'],
                'empName': emps[randint(0, len(emps) - 1)]['empName']
            })
        else:
            at = 0
            bindDepAssetHttp = post(url + 'department/bind/asset', headers={
                'sno': asset['sno'],
                'depName': deps[randint(0, len(deps) - 1)]['depName']
            })

        # ^ Binding asset to the type of asset it is
        bindAssetAssetType = post(url + 'asset/bind/asset-type', headers={
            'type': ats[at]['type'],
            'sno': asset['sno']
        })
    return assets
