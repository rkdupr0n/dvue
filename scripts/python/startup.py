from requests import post

# ^ Primitive post functions


def __postAt(url, assetType):
    return post(url + 'asset-type/add', json=assetType).json()


def __postDep(url, depName):
    return post(url + 'department/add', json={'depName': depName}).json()


def __postOrg(url, orgName):
    return post(url + 'organization/add', json={'orgName': orgName}).json()


def __bindDepToOrg(url, depOrg):
    res = post(url + 'organization/bind/department', headers=depOrg)


# ^ Multiple post functions

def __postAssetTypes(url, assetTypes):
    ats = []
    for i in assetTypes:
        ats.append(__postAt(url, i))
    return ats


def __postDepartments(url, depNames):
    deps = []
    for i in depNames:
        deps.append(__postDep(url, i))
    return deps


def __postOrganizations(url, orgNames):
    orgs = []
    for i in orgNames:
        orgs.append(__postOrg(url, i))
    return orgs


def __bindDepsToOrgs(url, depOrgs):
    for i in depOrgs:
        __bindDepToOrg(url, i)


# ^ Initialization Function

def initDB(url):
    assetTypes = [
        {
            'type': 'Department',
            'description': 'Assets that are kept track of by the department'
        },
        {
            'type': 'Employee',
            'description': 'Personal assets owned by the employee.'
        }
    ]
    depNames = ['Marketing', 'Research', 'Administrative']
    orgNames = ['SIEMENS']

    depOrgs = []
    for i in orgNames:
        for j in depNames:
            depOrgs.append({'orgName': i, 'depName': j})

    ats = __postAssetTypes(url, assetTypes)
    deps = __postDepartments(url, depNames)
    orgs = __postOrganizations(url, orgNames)
    __bindDepsToOrgs(url, depOrgs)
    
    return ats, deps, orgs
