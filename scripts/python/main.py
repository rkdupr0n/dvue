from json import load
from startup import initDB
from postUtilities import postEmps, postAssets

cfgFile = open('config.json')
cfg = load(cfgFile)

ats, deps, orgs = initDB(cfg['postBaseUrl'])

emps = postEmps(cfg['postBaseUrl'], cfg['numOfEmps'], deps)
assets = postAssets(cfg['postBaseUrl'], cfg['numOfAssets'], deps, emps, ats)
