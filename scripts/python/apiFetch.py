from requests import get

# Not yet configured to be used
def getFakeOrgs(count):
    return get('https://fakerapi.it/api/v1/custom?_quantity=' + str(count) + '&orgName=company_name')


def getFakeEmps(count):
    return get('https://fakerapi.it/api/v1/custom?_quantity=' + str(count) +
               '&name=firstName&surname=lastName&email=email&phone=phone').json()


def getFakeAssets(count):
    return get('http://names.drycodes.com/' +
               str(count) + '?nameOptions=objects').json()
