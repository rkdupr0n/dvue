package org.siemens.dvue.assetms.api;

import java.net.URI;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/asset")
public class AssetController {

    @Autowired
    private RestTemplate template;

    @Value("${asset.baseUrl}")
    private String baseUrl;

    // $ Adding new Asset
    @PostMapping(value = "/add")
    public ResponseEntity<AppAsset> addAsset(@RequestBody AppAsset asset) {
        URI uri = URI.create(baseUrl + "/add");
        return ResponseEntity.created(uri).body(template.postForObject(uri, asset, AppAsset.class));
    }

    // $ Getting all respective Assets
    @GetMapping(value = "/all")
    @SuppressWarnings (value="unchecked")
    public ResponseEntity<Map<String, Object>> getAllAssets(@RequestHeader(defaultValue = "0") String page,
    @RequestHeader(defaultValue = "5") String size) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("page", page);
        headers.add("size", size);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/all").headers(headers).build();
        Map<String, Object> res = template.exchange(req, Map.class).getBody();
        return ResponseEntity.ok(res);
    }

    // $ Getting a specific instance of an Asset
    @GetMapping(value = "/get")
    public ResponseEntity<AppAsset> getAsset(@RequestHeader String sno) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("sno", sno);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get").headers(headers).build();
        return template.exchange(req, AppAsset.class);
    }

    // $ Asset specific
    @GetMapping(value = "/check-validity")
    public ResponseEntity<Boolean> checkAssetValidity(@RequestHeader String sno, @RequestHeader String date) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("sno", sno);
        headers.add("date", date);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/check-validity").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Binding entities
    @PostMapping(value = "/bind/asset-type")
    public ResponseEntity<Boolean> bindAssetTypeToAsset(@RequestHeader String type, @RequestHeader String sno) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("type", type);
        headers.add("sno", sno);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/bind/asset-type").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Updating Asset
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateAsset(@RequestHeader String sno, @RequestBody AppAsset asset) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("sno", sno);
        RequestEntity<AppAsset> req = RequestEntity.post(baseUrl + "/update").headers(headers).body(asset);
        return template.exchange(req, Boolean.class);
    }

    // $ Deleting Asset
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteAsset(@RequestHeader String sno) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("sno", sno);
        RequestEntity<Void> req = RequestEntity.delete(baseUrl + "/delete").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Unbinding entities
    @PostMapping(value = "/unbind/asset-type")
    public ResponseEntity<Boolean> unbindAssetTypeFromAsset(@RequestHeader String sno) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("sno", sno);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/unbind/asset-type").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }
}
