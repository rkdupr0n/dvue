package org.siemens.dvue.dbservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableRetry
public class DvueDbServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(DvueDbServiceApplication.class, args);
	}
}
