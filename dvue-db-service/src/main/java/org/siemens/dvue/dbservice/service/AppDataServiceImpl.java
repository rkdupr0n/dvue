package org.siemens.dvue.dbservice.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.siemens.dvue.dbservice.common.IsEmpty;
import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppAssetType;
import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppEmployee;
import org.siemens.dvue.dbservice.domain.AppOrganization;
import org.siemens.dvue.dbservice.repo.AppAssetRepo;
import org.siemens.dvue.dbservice.repo.AppAssetTypeRepo;
import org.siemens.dvue.dbservice.repo.AppDepartmentRepo;
import org.siemens.dvue.dbservice.repo.AppEmployeeRepo;
import org.siemens.dvue.dbservice.repo.AppOrganizationRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
@Transactional
public class AppDataServiceImpl implements AppDataService {
    private final AppOrganizationRepo orgRepo;
    private final AppDepartmentRepo depRepo;
    private final AppEmployeeRepo empRepo;
    private final AppAssetRepo assetRepo;
    private final AppAssetTypeRepo astyRepo;

    // $ Adding new entities
    @Override
    public AppOrganization addOrganization(AppOrganization organization) {
        return orgRepo.save(organization);
    }

    @Override
    public AppDepartment addDepartment(AppDepartment department) {
        return depRepo.save(department);
    }

    @Override
    public AppEmployee addEmployee(AppEmployee employee) {
        return empRepo.save(employee);
    }

    @Override
    public AppAsset addAsset(AppAsset asset) {
        return assetRepo.save(asset);
    }

    @Override
    public AppAssetType addAssetType(AppAssetType assetType) {
        return astyRepo.save(assetType);
    }

    // $ Getting all respective entities
    @Override
    public Page<AppOrganization> getAllOrganizations(PageRequest pageRequest) {
        return orgRepo.findAll(pageRequest);
    }

    @Override
    public Page<AppDepartment> getAllDepartments(PageRequest pageRequest) {
        return depRepo.findAll(pageRequest);
    }

    @Override
    public Page<AppEmployee> getAllEmployees(PageRequest pageRequest) {
        return empRepo.findAll(pageRequest);
    }

    @Override
    public Page<AppAsset> getAllAssets(PageRequest pageRequest) {
        return assetRepo.findAll(pageRequest);
    }

    @Override
    public Page<AppAssetType> getAllAssetTypes(PageRequest pageRequest) {
        return astyRepo.findAll(pageRequest);
    }

    // $ Getting a specific instances of an entity
    @Override
    public AppOrganization getOrganization(String orgName) {
        return orgRepo.findByOrgName(orgName);
    }

    @Override
    public AppDepartment getDepartment(String depName) {
        return depRepo.findByDepName(depName);
    }

    @Override
    public AppEmployee getEmployee(String empName) {
        return empRepo.findByEmpName(empName);
    }

    @Override
    public AppAsset getAsset(String sno) {
        return assetRepo.findBySno(sno);
    }

    @Override
    public AppAssetType getAssetType(String type) {
        return astyRepo.findByType(type);
    }

    // $ Organization specific
    @Override
    public List<AppDepartment> getOrganizationDepartments(String orgName) {
        // ^ The toList() method won't be available when using older versions of java
        final AppOrganization org = getOrganization(orgName);
        if (IsEmpty.check(org) || IsEmpty.check(org.getDepartments()))
            return List.of();
        return org.getDepartments().stream().toList();
    }

    // $ Department specific
    @Override
    public List<AppEmployee> getDepartmentEmployees(String depName) {
        final AppDepartment dep = getDepartment(depName);
        if (IsEmpty.check(dep) || IsEmpty.check(dep.getEmployees()))
            return List.of();
        return dep.getEmployees().stream().toList();
    }

    @Override
    public List<AppAsset> getDepartmentAssets(String depName) {
        final AppDepartment dep = getDepartment(depName);
        if (IsEmpty.check(dep) || IsEmpty.check(dep.getAssets()))
            return List.of();
        return dep.getAssets().stream().toList();
    }

    AppOrganization getDepartmentOrganization(AppDepartment dep) {
        if (IsEmpty.check(dep) || IsEmpty.check(dep.getOrgId()))
            return new AppOrganization();
        return orgRepo.getById(dep.getOrgId());
    }

    @Override
    public AppOrganization getDepartmentOrganization(String depName) {
        final AppDepartment dep = getDepartment(depName);
        return getDepartmentOrganization(dep);
    }

    // $ Employee specific
    @Override
    public List<AppAsset> getEmployeeAssets(String empName) {
        final AppEmployee emp = getEmployee(empName);
        if (IsEmpty.check(emp) || IsEmpty.check(emp.getAssets()))
            return List.of();
        return emp.getAssets().stream().toList();
    }

    AppDepartment getEmployeeDepartment(AppEmployee emp) {
        if (IsEmpty.check(emp) || IsEmpty.check(emp.getDepId()))
            return new AppDepartment();
        return depRepo.getById(emp.getDepId());
    }

    @Override
    public AppDepartment getEmployeeDepartment(String empName) {
        final AppEmployee emp = getEmployee(empName);
        return getEmployeeDepartment(emp);
    }

    // $ Asset specific
    @Override
    public Boolean checkAssetValidity(String sno, Date date) {
        // TODO: Get this functionality to work
        AppAsset asset = getAsset(sno);
        if (IsEmpty.check(asset) || IsEmpty.check(asset.getEndDate()) || IsEmpty.check(asset.getStartDate()))
            return false;
        return date.compareTo(asset.getEndDate()) < 0 && date.compareTo(asset.getStartDate()) > 0;
        // return date.before(asset.getEndDate()) && date.after(asset.getStartDate());
    }

    // $ AssetType specific
    @Override
    public String getAssetTypeDescription(String type) {
        final AppAssetType asty = getAssetType(type);
        if (IsEmpty.check(asty) || IsEmpty.check(asty.getDescription()))
            return "";
        return asty.getDescription();
    }

    // $ Binding entities
    @Override
    public Boolean bindDepartmentToOrganization(String depName, String orgName) {
        AppDepartment dep = getDepartment(depName);
        AppOrganization org = getOrganization(orgName);
        if (IsEmpty.check(org) || IsEmpty.check(dep) || !IsEmpty.check(dep.getOrgId()))
            return false;
        if (org.getDepartments().contains(dep))
            return true;
        dep.setOrgId(org.getId());
        return org.getDepartments().add(dep);
    }

    @Override
    public Boolean bindEmployeeToDepartment(String empName, String depName) {
        AppEmployee emp = getEmployee(empName);
        AppDepartment dep = getDepartment(depName);
        if (IsEmpty.check(emp) || IsEmpty.check(dep) || !IsEmpty.check(emp.getDepId()))
            return false;
        if (dep.getEmployees().contains(emp))
            return true;
        emp.setDepId(dep.getId());
        return dep.getEmployees().add(emp);
    }

    @Override
    public Boolean bindAssetToDepartment(String sno, String depName) {
        AppAsset asset = getAsset(sno);
        AppDepartment dep = getDepartment(depName);
        if (IsEmpty.check(asset) || IsEmpty.check(dep))
            return false;
        if (dep.getAssets().contains(asset))
            return true;
        return dep.getAssets().add(asset);
    }

    @Override
    public Boolean bindAssetToEmployee(String sno, String empName) {
        AppAsset asset = getAsset(sno);
        AppEmployee emp = getEmployee(empName);
        if (IsEmpty.check(asset) || IsEmpty.check(emp))
            return false;
        if (emp.getAssets().contains(asset))
            return true;
        return emp.getAssets().add(asset);
    }

    @Override
    public Boolean bindAssetTypeToAsset(String type, String sno) {
        AppAssetType aType = getAssetType(type);
        AppAsset asset = getAsset(sno);
        if (IsEmpty.check(asset) || IsEmpty.check(aType))
            return false;
        if (!IsEmpty.check(asset.getAssetType()))
            return false;
        asset.setAssetType(aType);
        assetRepo.saveAndFlush(asset);
        return true;
    }

    // $ Updating entities
    @Override
    public Boolean updateOrganization(String orgName, AppOrganization organization) {
        AppOrganization org = getOrganization(orgName);

        if (IsEmpty.check(org) || (!IsEmpty.check(organization.getId()) && !org.getId().equals(organization.getId())))
            return false;

        Collection<AppDepartment> deps = organization.getDepartments();
        String name = organization.getOrgName();

        org.setDepartments(IsEmpty.check(deps) ? org.getDepartments() : deps);
        org.setOrgName(IsEmpty.check(name) ? org.getOrgName() : name);

        return true;
    }

    @Override
    public Boolean updateDepartment(String depName, AppDepartment department) {
        AppDepartment dep = getDepartment(depName);

        if (IsEmpty.check(dep) || (!IsEmpty.check(department.getId()) && !dep.getId().equals(department.getId())))
            return false;

        Collection<AppAsset> assets = department.getAssets();
        String name = department.getDepName();
        Collection<AppEmployee> emps = department.getEmployees();
        Long orgId = department.getOrgId();

        dep.setAssets(IsEmpty.check(assets) ? dep.getAssets() : assets);
        dep.setDepName(IsEmpty.check(name) ? dep.getDepName() : name);
        dep.setEmployees(IsEmpty.check(emps) ? dep.getEmployees() : emps);
        dep.setOrgId(IsEmpty.check(orgId) ? dep.getOrgId() : orgId);

        return true;
    }

    @Override
    public Boolean updateEmployee(String empName, AppEmployee employee) {
        AppEmployee emp = getEmployee(empName);

        if (IsEmpty.check(emp) || (!IsEmpty.check(employee.getDepId()) && !emp.getGid().equals(employee.getGid())))
            return false;

        Collection<AppAsset> assets = employee.getAssets();
        Long depId = employee.getDepId();
        String email = employee.getEmail();
        String name = employee.getEmpName();
        String phoneNo = emp.getPhoneNo();

        emp.setAssets(IsEmpty.check(assets) ? emp.getAssets() : assets);
        emp.setDepId(IsEmpty.check(depId) ? emp.getDepId() : depId);
        emp.setEmail(IsEmpty.check(email) ? emp.getEmail() : email);
        emp.setEmpName(IsEmpty.check(name) ? emp.getEmpName() : name);
        emp.setPhoneNo(IsEmpty.check(phoneNo) ? emp.getPhoneNo() : phoneNo);

        return true;
    }

    @Override
    public Boolean updateAsset(String sno, AppAsset asset) {
        AppAsset ast = getAsset(sno);

        if (IsEmpty.check(ast) || (!IsEmpty.check(asset.getSno()) && !ast.getSno().equals(asset.getSno())))
            return false;

        AppAssetType asType = asset.getAssetType();
        String name = asset.getName();
        Date endDate = asset.getEndDate();
        Date startDate = asset.getStartDate();

        ast.setName(IsEmpty.check(name) ? ast.getName() : name);
        ast.setAssetType(IsEmpty.check(asType) ? ast.getAssetType() : asType);
        ast.setEndDate(IsEmpty.check(endDate) ? ast.getEndDate() : endDate);
        ast.setStartDate(IsEmpty.check(startDate) ? ast.getStartDate() : startDate);

        return true;
    }

    @Override
    public Boolean updateAssetType(String type, AppAssetType assetType) {
        AppAssetType asty = getAssetType(type);

        if (IsEmpty.check(asty) || (!IsEmpty.check(assetType.getId()) && !asty.getId().equals(assetType.getId())))
            return false;

        String desc = assetType.getDescription();
        String nm = assetType.getType();

        asty.setDescription(IsEmpty.check(desc) ? asty.getDescription() : desc);
        asty.setType(IsEmpty.check(nm) ? asty.getType() : nm);

        return true;
    }

    // $ Deleting entities
    @Override
    public Boolean deleteOrganization(String orgName) {
        final AppOrganization org = getOrganization(orgName);
        if (IsEmpty.check(org))
            return false;
        // var noErr = new Object() {
        // boolean val = true;
        // };
        org.getDepartments().forEach(dep -> {
            dep.setOrgId(null);
            // if (!deleteDepartment(dep))
            // noErr.val = false;
        });
        orgRepo.delete(org);
        return true;
        // return noErr.val;
    }

    Boolean deleteDepartment(AppDepartment dep) {
        if (IsEmpty.check(dep))
            return false;
        // dep.getAssets().forEach(asset -> {
        // assetRepo.delete(asset);
        // });
        final AppOrganization org = getDepartmentOrganization(dep);
        if (!IsEmpty.check(org) && !IsEmpty.check(org.getDepartments()))
            org.getDepartments().remove(dep);
        dep.getEmployees().forEach(emp -> {
            emp.setDepId(null);
            // empRepo.delete(emp);
        });
        depRepo.delete(dep);
        return true;
    }

    @Override
    public Boolean deleteDepartment(String depName) {
        final AppDepartment dep = getDepartment(depName);
        return deleteDepartment(dep);
    }

    @Override
    public Boolean deleteEmployee(String empName) {
        final AppEmployee emp = getEmployee(empName);
        if (IsEmpty.check(emp))
            return false;
        final AppDepartment dep = getEmployeeDepartment(emp);
        // TODO: Optimizations can be made here and other similar areas by using 2
        // seperate conditions
        // instead of calling dep.getEmployees() twice.
        if (!IsEmpty.check(dep) && !IsEmpty.check(dep.getEmployees()))
            dep.getEmployees().remove(emp);
        // emp.getAssets().forEach(asset -> {
        // assetRepo.delete(asset);
        // });
        empRepo.delete(emp);
        return true;
    }

    Boolean deleteAsset(AppAsset asset) {
        if (IsEmpty.check(asset))
            return false;
        depRepo.findByAssetsContaining(asset).forEach(dep -> {
            dep.getAssets().remove(asset);
        });
        empRepo.findByAssetsContaining(asset).forEach(emp -> {
            emp.getAssets().remove(asset);
        });
        assetRepo.delete(asset);
        return true;
    }

    @Override
    public Boolean deleteAsset(String sno) {
        final AppAsset asset = getAsset(sno);
        return deleteAsset(asset);
    }

    @Override
    public Boolean deleteAssetType(String type) {
        final AppAssetType assetType = getAssetType(type);
        if (IsEmpty.check(assetType))
            return false;
        // var noErr = new Object() {
        // boolean val = true;
        // };
        assetRepo.findByAssetType(assetType).forEach(asset -> {
            asset.setAssetType(null);
            // if (!deleteAsset(asset))
            // noErr.val = false;
        });
        astyRepo.delete(assetType);
        // return noErr.val;
        return true;
    }

    // $ Unbinding entities
    @Override
    public Boolean unbindDepartmentFromOrganization(String depName) {
        AppDepartment dep = getDepartment(depName);
        if (IsEmpty.check(dep))
            return false;
        boolean valid = orgRepo.getById(dep.getOrgId()).getDepartments().remove(dep);
        dep.setOrgId(null);
        return valid;
    }

    @Override
    public Boolean unbindEmployeeFromDepartment(String empName) {
        AppEmployee emp = getEmployee(empName);
        if (IsEmpty.check(emp))
            return false;
        boolean valid = depRepo.getById(emp.getDepId()).getEmployees().remove(emp);
        emp.setDepId(null);
        return valid;
    }

    @Override
    public Boolean unbindAssetFromDepartment(String depName, String sno) {
        AppDepartment dep = getDepartment(depName);
        if (IsEmpty.check(dep))
            return false;
        AppAsset asset = assetRepo.getById(sno);
        return dep.getAssets().remove(asset);
    }

    @Override
    public Boolean unbindAssetFromEmployee(String empName, String sno) {
        AppEmployee emp = getEmployee(empName);
        AppAsset asset = getAsset(sno);
        if (IsEmpty.check(emp) || IsEmpty.check(asset))
            return false;
        return emp.getAssets().remove(asset);
    }

    @Override
    public Boolean unbindAssetTypeFromAsset(String sno) {
        AppAsset asset = getAsset(sno);
        if (IsEmpty.check(asset))
            return false;
        asset.setAssetType(null);
        return true;
    }
}
