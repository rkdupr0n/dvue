package org.siemens.dvue.dbservice.repo;

import java.util.List;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppEmployee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppEmployeeRepo extends JpaRepository<AppEmployee, Long> {
	AppEmployee findByEmpName(String empName);

	AppEmployee findByEmail(String email);

	AppEmployee findByPhoneNo(String phoneNo);

	List<AppEmployee> findByAssetsContaining(AppAsset asset);
}
