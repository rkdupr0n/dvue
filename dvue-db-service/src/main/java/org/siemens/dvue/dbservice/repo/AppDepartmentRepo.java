package org.siemens.dvue.dbservice.repo;

import java.util.List;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppEmployee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppDepartmentRepo extends JpaRepository<AppDepartment, Long> {
	AppDepartment findByDepName(String depName);

	AppDepartment findByEmployeesContaining(AppEmployee employee);

	List<AppDepartment> findByAssetsContaining(AppAsset employee);
}
