package org.siemens.dvue.dbservice.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppEmployee {
    @Id
    @GeneratedValue
    private Long gid;
    private String empName;
    private String email;
    private String phoneNo;
    private Long depId;
    @OneToMany
    private Collection<AppAsset> assets = new ArrayList<>();
}
