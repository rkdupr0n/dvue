package org.siemens.dvue.dbservice.api;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppEmployee;
import org.siemens.dvue.dbservice.domain.AppOrganization;
import org.siemens.dvue.dbservice.service.AppDataService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/department")
@RequiredArgsConstructor
public class DbDepartmentController {
    private final AppDataService dataService;

    // $ Adding new Department
    @PostMapping(value = "/add")
    public ResponseEntity<AppDepartment> addDepartment(@RequestBody AppDepartment department) {
        URI uri = URI
                .create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/department/add").toUriString());
        return ResponseEntity.created(uri).body(dataService.addDepartment(department));
    }

    // $ Getting all respective Departments
    @GetMapping(value = "/all")
    public ResponseEntity<Map<String, Object>> getAllDepartments(@RequestHeader(defaultValue = "0") Integer page,
    @RequestHeader(defaultValue = "5") Integer size) {
        Map<String, Object> response = new HashMap<>();
        Page<AppDepartment> appPage = dataService.getAllDepartments(PageRequest.of(page, size));

        response.put("content", appPage.getContent());
        response.put("currentPage", appPage.getNumber());
        response.put("totalItems", appPage.getTotalElements());
        response.put("totalPages", appPage.getTotalPages());

        return ResponseEntity.ok(response);
    }

    // $ Getting a specific instance of a Department
    @GetMapping(value = "/get")
    public ResponseEntity<AppDepartment> getDepartment(@RequestHeader String depName) {
        return ResponseEntity.ok(dataService.getDepartment(depName));
    }

    // $ Department specific
    @GetMapping(value = "/get/assets")
    public ResponseEntity<List<AppAsset>> getDepartmentAssets(@RequestHeader String depName) {
        return ResponseEntity.ok(dataService.getDepartmentAssets(depName));
    }

    @GetMapping(value = "/get/employees")
    public ResponseEntity<List<AppEmployee>> getDepartmentEmployees(@RequestHeader String depName) {
        return ResponseEntity.ok(dataService.getDepartmentEmployees(depName));
    }

    @GetMapping(value = "/get/organization")
    public ResponseEntity<AppOrganization> getDepartmentOrganization(@RequestHeader String depName) {
        return ResponseEntity.ok(dataService.getDepartmentOrganization(depName));
    }

    // $ Binding entities
    @PostMapping(value = "/bind/asset")
    public ResponseEntity<Boolean> bindAssetToDepartment(@RequestHeader String sno, @RequestHeader String depName) {
        return ResponseEntity.ok(dataService.bindAssetToDepartment(sno, depName));
    }

    @PostMapping(value = "/bind/employee")
    public ResponseEntity<Boolean> bindEmployeeToDepartment(@RequestHeader String empName,
            @RequestHeader String depName) {
        return ResponseEntity.ok(dataService.bindEmployeeToDepartment(empName, depName));
    }

    // $ Updating Department
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateDepartment(@RequestHeader String depName,
            @RequestBody AppDepartment department) {
        return ResponseEntity.ok(dataService.updateDepartment(depName, department));
    }

    // $ Deleting Department
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteDepartment(@RequestHeader String depName) {
        return ResponseEntity.ok(dataService.deleteDepartment(depName));
    }

    // $ Unbinding entities
    @PostMapping(value = "/unbind/asset")
    public ResponseEntity<Boolean> unbindAssetFromDepartment(@RequestHeader String depName, @RequestHeader String sno) {
        return ResponseEntity.ok(dataService.unbindAssetFromDepartment(depName, sno));
    }

    @PostMapping(value = "/unbind/employee")
    public ResponseEntity<Boolean> unbindEmployeeFromDepartment(@RequestHeader String empName) {
        return ResponseEntity.ok(dataService.unbindEmployeeFromDepartment(empName));
    }
}

