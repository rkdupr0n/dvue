package org.siemens.dvue.dbservice.common;

import java.util.Collection;

public class IsEmpty {
    public static boolean check(String s) {
        return s == null || s == "";
    }

    public static boolean check(Collection<?> s) {
        return s == null || s.isEmpty();
    }

    public static boolean check(Object s) {
        return s == null;
    }

    public static String otherwise(String s, String other) {
        return check(s) ? other : s;
    }
}
