package org.siemens.dvue.dbservice.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppOrganization {
	@Id
	@GeneratedValue
	private Long id;
	private String orgName;
	@OneToMany
	private Collection<AppDepartment> departments = new ArrayList<>();
}
