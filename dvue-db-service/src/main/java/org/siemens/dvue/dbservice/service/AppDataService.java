package org.siemens.dvue.dbservice.service;

import java.util.Date;
import java.util.List;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppAssetType;
import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppEmployee;
import org.siemens.dvue.dbservice.domain.AppOrganization;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface AppDataService {
    // $ Adding new entities
    AppOrganization addOrganization(AppOrganization organization);

    AppDepartment addDepartment(AppDepartment department);

    AppEmployee addEmployee(AppEmployee employee);

    AppAsset addAsset(AppAsset asset);

    AppAssetType addAssetType(AppAssetType assetType);

    // $ Getting all respective entities
    Page<AppOrganization> getAllOrganizations(PageRequest pageRequest);

    Page<AppDepartment> getAllDepartments(PageRequest pageRequest);

    Page<AppEmployee> getAllEmployees(PageRequest pageRequest);

    Page<AppAsset> getAllAssets(PageRequest pageRequest);

    Page<AppAssetType> getAllAssetTypes(PageRequest pageRequest);

    // $ Getting a specific instance of an entity
    AppOrganization getOrganization(String orgName);

    AppDepartment getDepartment(String depName);

    AppEmployee getEmployee(String empName);

    AppAsset getAsset(String sno);

    AppAssetType getAssetType(String type);

    // $ Organization specific
    List<AppDepartment> getOrganizationDepartments(String orgName);

    // $ Department specific
    List<AppEmployee> getDepartmentEmployees(String depName);

    List<AppAsset> getDepartmentAssets(String depName);

    AppOrganization getDepartmentOrganization(String depName);

    // $ Employee specific
    List<AppAsset> getEmployeeAssets(String empName);

    AppDepartment getEmployeeDepartment(String empName);

    // $ Asset specific
    Boolean checkAssetValidity(String sno, Date date);

    // $ AssetType specific
    String getAssetTypeDescription(String type);

    // $ Binding entities
    Boolean bindDepartmentToOrganization(String depName, String orgName);

    Boolean bindEmployeeToDepartment(String empName, String depName);

    Boolean bindAssetToDepartment(String sno, String depName);

    Boolean bindAssetToEmployee(String sno, String empName);

    Boolean bindAssetTypeToAsset(String type, String sno);

    // $ Updating entities
    Boolean updateOrganization(String orgName, AppOrganization organization);

    Boolean updateDepartment(String depName, AppDepartment department);

    Boolean updateEmployee(String empName, AppEmployee employee);

    Boolean updateAsset(String sno, AppAsset asset);

    Boolean updateAssetType(String type, AppAssetType assetType);

    // $ Deleting entities
    Boolean deleteOrganization(String orgName);

    Boolean deleteDepartment(String depName);

    Boolean deleteEmployee(String empName);

    Boolean deleteAsset(String sno);

    Boolean deleteAssetType(String type);

    // $ Unbinding entities
    Boolean unbindDepartmentFromOrganization(String depName);

    Boolean unbindEmployeeFromDepartment(String empName);

    Boolean unbindAssetFromDepartment(String depName, String sno);

    Boolean unbindAssetFromEmployee(String empName, String sno);

    Boolean unbindAssetTypeFromAsset(String sno);
}
