package org.siemens.dvue.dbservice.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppDepartment {
    @Id
    @GeneratedValue
	private Long id;
    private String depName;
    private Long orgId;
    @OneToMany
    private Collection<AppEmployee> employees = new ArrayList<>();
    @OneToMany
    private Collection<AppAsset> assets = new ArrayList<>();
}
