package org.siemens.dvue.dbservice.api;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.service.AppDataService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/asset")
@RequiredArgsConstructor
public class DbAssetController {
    private final AppDataService dataService;

    // $ Adding new Asset
    @PostMapping(value = "/add")
    public ResponseEntity<AppAsset> addAsset(@RequestBody AppAsset asset) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/asset/add").toUriString());
        return ResponseEntity.created(uri).body(dataService.addAsset(asset));
    }

    // $ Getting all respective Assets
    @GetMapping(value = "/all")
    public ResponseEntity<Map<String, Object>> getAllAssets(@RequestHeader(defaultValue = "0") Integer page,
    @RequestHeader(defaultValue = "5") Integer size) {
        Map<String, Object> response = new HashMap<>();
        Page<AppAsset> appPage = dataService.getAllAssets(PageRequest.of(page, size));

        response.put("content", appPage.getContent());
        response.put("currentPage", appPage.getNumber());
        response.put("totalItems", appPage.getTotalElements());
        response.put("totalPages", appPage.getTotalPages());

        return ResponseEntity.ok(response);
    }

    // $ Getting a specific instance of an Asset
    @GetMapping(value = "/get")
    public ResponseEntity<AppAsset> getAsset(@RequestHeader String sno) {
        return ResponseEntity.ok(dataService.getAsset(sno));
    }

    // $ Asset specific
    @GetMapping(value = "/check-validity")
    public ResponseEntity<Boolean> checkAssetValidity(@RequestHeader String sno, @RequestHeader String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date fDate = new GregorianCalendar(42069, 10, 20).getTime();
        try {
            fDate = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(dataService.checkAssetValidity(sno, fDate));
    }

    // $ Binding entities
    @PostMapping(value = "/bind/asset-type")
    public ResponseEntity<Boolean> bindAssetTypeToAsset(@RequestHeader String type, @RequestHeader String sno) {
        return ResponseEntity.ok(dataService.bindAssetTypeToAsset(type, sno));
    }

    // $ Updating Asset
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateAsset(@RequestHeader String sno, @RequestBody AppAsset asset) {
        return ResponseEntity.ok(dataService.updateAsset(sno, asset));
    }

    // $ Deleting Asset
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteAsset(@RequestHeader String sno) {
        return ResponseEntity.ok(dataService.deleteAsset(sno));
    }

    // $ Unbinding entities
    @PostMapping(value = "/unbind/asset-type")
    public ResponseEntity<Boolean> unbindAssetTypeFromAsset(@RequestHeader String sno) {
        return ResponseEntity.ok(dataService.unbindAssetTypeFromAsset(sno));
    }
}
