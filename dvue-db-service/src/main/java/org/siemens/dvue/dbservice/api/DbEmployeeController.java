package org.siemens.dvue.dbservice.api;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppEmployee;
import org.siemens.dvue.dbservice.service.AppDataService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/employee")
@RequiredArgsConstructor
public class DbEmployeeController {
    private final AppDataService dataService;

    // $ Adding new Employee
    @PostMapping(value = "/add")
    public ResponseEntity<AppEmployee> addEmployee(@RequestBody AppEmployee employee) {
        URI uri = URI
                .create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/employee/add").toUriString());
        return ResponseEntity.created(uri).body(dataService.addEmployee(employee));
    }

    // $ Getting all respective Employees
    @GetMapping(value = "/all")
    public ResponseEntity<Map<String, Object>> getAllEmployees(@RequestHeader(defaultValue = "0") Integer page,
    @RequestHeader(defaultValue = "5") Integer size) {
        Map<String, Object> response = new HashMap<>();
        Page<AppEmployee> appPage = dataService.getAllEmployees(PageRequest.of(page, size));

        response.put("content", appPage.getContent());
        response.put("currentPage", appPage.getNumber());
        response.put("totalItems", appPage.getTotalElements());
        response.put("totalPages", appPage.getTotalPages());

        return ResponseEntity.ok(response);
    }

    // $ Getting a specific instance of an Employee
    @GetMapping(value = "/get")
    public ResponseEntity<AppEmployee> getEmployee(@RequestHeader String empName) {
        return ResponseEntity.ok(dataService.getEmployee(empName));
    }

    // $ Employee specific
    @GetMapping(value = "/get/assets")
    public ResponseEntity<List<AppAsset>> getEmployeeAssets(@RequestHeader String empName) {
        return ResponseEntity.ok(dataService.getEmployeeAssets(empName));
    }

    @GetMapping(value = "/get/department")
    public ResponseEntity<AppDepartment> getEmployeeDepartment(@RequestHeader String empName) {
        return ResponseEntity.ok(dataService.getEmployeeDepartment(empName));
    }

    // $ Binding entities
    @PostMapping(value = "/bind/asset")
    public ResponseEntity<Boolean> bindAssetToEmployee(@RequestHeader String sno, @RequestHeader String empName) {
        return ResponseEntity.ok(dataService.bindAssetToEmployee(sno, empName));
    }

    // $ Updating Employee
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateEmployee(@RequestHeader String empName, @RequestBody AppEmployee employee) {
        return ResponseEntity.ok(dataService.updateEmployee(empName, employee));
    }

    // $ Deleting Employee
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteEmployee(@RequestHeader String empName) {
        return ResponseEntity.ok(dataService.deleteEmployee(empName));
    }

    // $ Unbinding entities
    @PostMapping(value = "/unbind/asset")
    public ResponseEntity<Boolean> unbindAssetFromEmployee(@RequestHeader String empName, @RequestHeader String sno) {
        return ResponseEntity.ok(dataService.unbindAssetFromEmployee(empName, sno));
    }
}
