package org.siemens.dvue.dbservice.api;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppOrganization;
import org.siemens.dvue.dbservice.service.AppDataService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/organization")
@RequiredArgsConstructor
public class DbOrganizationController {
    private final AppDataService dataService;

    // $ Adding new Organization
    @PostMapping(value = "/add")
    public ResponseEntity<AppOrganization> addOrganization(@RequestBody AppOrganization organization) {
        URI uri = URI.create(
                ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/organization/add").toUriString());
        return ResponseEntity.created(uri).body(dataService.addOrganization(organization));
    }

    // $ Getting all respective Organizations
    @GetMapping(value = "/all")
    public ResponseEntity<Map<String, Object>> getAllOrganizations(@RequestHeader(defaultValue = "0") Integer page,
    @RequestHeader(defaultValue = "5") Integer size) {
        Map<String, Object> response = new HashMap<>();
        Page<AppOrganization> appPage = dataService.getAllOrganizations(PageRequest.of(page, size));

        response.put("content", appPage.getContent());
        response.put("currentPage", appPage.getNumber());
        response.put("totalItems", appPage.getTotalElements());
        response.put("totalPages", appPage.getTotalPages());

        return ResponseEntity.ok(response);
    }

    // $ Getting a specific instance of an Organization
    @GetMapping(value = "/get")
    public ResponseEntity<AppOrganization> getOrganization(@RequestHeader String orgName) {
        return ResponseEntity.ok(dataService.getOrganization(orgName));
    }

    // $ Organization specific
    @GetMapping(value = "/get/departments")
    public ResponseEntity<List<AppDepartment>> getOrganizationDepartments(@RequestHeader String orgName) {
        return ResponseEntity.ok(dataService.getOrganizationDepartments(orgName));
    }

    // $ Binding entities
    @PostMapping(value = "/bind/department")
    public ResponseEntity<Boolean> bindDepartmentToOrganization(@RequestHeader String depName,
            @RequestHeader String orgName) {
        return ResponseEntity.ok(dataService.bindDepartmentToOrganization(depName, orgName));
    }

    // $ Updating Organization
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateOrganization(@RequestHeader String orgName,
            @RequestBody AppOrganization organization) {
        return ResponseEntity.ok(dataService.updateOrganization(orgName, organization));
    }

    // $ Deleting Organization
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteOrganization(@RequestHeader String orgName) {
        return ResponseEntity.ok(dataService.deleteOrganization(orgName));
    }

    // $ Unbinding entities
    @PostMapping(value = "/unbind/department")
    public ResponseEntity<Boolean> unbindDepartmentFromOrganization(@RequestHeader String depName) {
        return ResponseEntity.ok(dataService.unbindDepartmentFromOrganization(depName));
    }
}

