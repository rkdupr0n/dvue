package org.siemens.dvue.dbservice.repo;

import java.util.Date;
import java.util.List;

import org.siemens.dvue.dbservice.domain.AppAsset;
import org.siemens.dvue.dbservice.domain.AppAssetType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppAssetRepo extends JpaRepository<AppAsset, String> {
	List<AppAsset> findByName(String name);

	AppAsset findBySno(String sno);

	List<AppAsset> findByEndDateBefore(Date date);

	List<AppAsset> findByEndDateAfter(Date date);

	List<AppAsset> findByStartDateBefore(Date date);

	List<AppAsset> findByStartDateAfter(Date date);

	List<AppAsset> findByAssetType(AppAssetType assetType);
}
