package org.siemens.dvue.dbservice.repo;

import java.util.List;

import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppOrganization;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppOrganizationRepo extends JpaRepository<AppOrganization, Long> {
	AppOrganization findByOrgName(String orgName);

	List<AppOrganization> findByDepartmentsContaining(AppDepartment department);
}
