package org.siemens.dvue.dbservice.api;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppAssetType;
import org.siemens.dvue.dbservice.service.AppDataService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/asset-type")
@RequiredArgsConstructor
public class DbAssetTypeController {
    private final AppDataService dataService;

    // $ Adding new AssetType
    @PostMapping(value = "/add")
    public ResponseEntity<AppAssetType> addAssetType(@RequestBody AppAssetType assetType) {
        URI uri = URI
                .create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/asset-type/add").toUriString());
        return ResponseEntity.created(uri).body(dataService.addAssetType(assetType));
    }

    // $ Getting all respective AssetTypes
    @GetMapping(value = "/all")
    public ResponseEntity<Map<String, Object>> getAllAssetTypes(@RequestHeader(defaultValue = "0") Integer page,
            @RequestHeader(defaultValue = "5") Integer size) {
        Map<String, Object> response = new HashMap<>();
        Page<AppAssetType> appPage = dataService.getAllAssetTypes(PageRequest.of(page, size));

        response.put("content", appPage.getContent());
        response.put("currentPage", appPage.getNumber());
        response.put("totalItems", appPage.getTotalElements());
        response.put("totalPages", appPage.getTotalPages());

        return ResponseEntity.ok(response);
    }

    // $ Getting a specific instance of an AssetType
    @GetMapping(value = "/get")
    public ResponseEntity<AppAssetType> getAssetType(@RequestHeader String type) {
        return ResponseEntity.ok(dataService.getAssetType(type));
    }

    // $ AssetType specific
    @GetMapping(value = "/desc")
    public ResponseEntity<String> getAssetTypeDescription(@RequestHeader String type) {
        return ResponseEntity.ok(dataService.getAssetTypeDescription(type));
    }

    // // $ Binding entities

    // $ Updating AssetType
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateAssetType(@RequestHeader String type, @RequestBody AppAssetType assetType) {
        return ResponseEntity.ok(dataService.updateAssetType(type, assetType));
    }

    // $ Deleting AssetType
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteAssetType(@RequestHeader String type) {
        return ResponseEntity.ok(dataService.deleteAssetType(type));
    }
    // // $ Unbinding entities
}
