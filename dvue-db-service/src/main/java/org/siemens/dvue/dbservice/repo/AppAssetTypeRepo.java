package org.siemens.dvue.dbservice.repo;

import org.siemens.dvue.dbservice.domain.AppAssetType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppAssetTypeRepo extends JpaRepository<AppAssetType, Long> {
	AppAssetType findByType(String type);
}
