package org.siemens.dvue.organizationms.api;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.siemens.dvue.dbservice.domain.AppDepartment;
import org.siemens.dvue.dbservice.domain.AppOrganization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/organization")
public class OrganizationController {

    @Autowired
    private RestTemplate template;

    @Value("${organization.baseUrl}")
    private String baseUrl;

    // $ Adding new Organization
    @PostMapping(value = "/add")
    public ResponseEntity<AppOrganization> addOrganization(@RequestBody AppOrganization organization) {
        URI uri = URI.create(baseUrl + "/add");
        return ResponseEntity.created(uri).body(template.postForObject(uri, organization, AppOrganization.class));
    }

    // $ Getting all respective Organizations
    @GetMapping(value = "/all")
    @SuppressWarnings(value = "unchecked")
    public ResponseEntity<Map<String, Object>> getAllOrganizations(@RequestHeader(defaultValue = "0") String page,
    @RequestHeader(defaultValue = "5") String size) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("page", page);
        headers.add("size", size);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/all").headers(headers).build();
        Map<String, Object> res = template.exchange(req, Map.class).getBody();
        return ResponseEntity.ok(res);
    }

    // $ Getting a specific instance of an Organization
    @GetMapping(value = "/get")
    public ResponseEntity<AppOrganization> getOrganization(@RequestHeader String orgName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("orgName", orgName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get").headers(headers).build();
        return template.exchange(req, AppOrganization.class);
    }

    // $ Organization specific
    @GetMapping(value = "/get/departments")
    public ResponseEntity<List<AppDepartment>> getOrganizationDepartments(@RequestHeader String orgName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("orgName", orgName);
        RequestEntity<Void> req = RequestEntity.get(baseUrl + "/get/departments").headers(headers).build();
        return ResponseEntity.ok(List.of(template.exchange(req, AppDepartment[].class).getBody()));
    }

    // $ Binding entities
    @PostMapping(value = "/bind/department")
    public ResponseEntity<Boolean> bindDepartmentToOrganization(@RequestHeader String depName,
            @RequestHeader String orgName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        headers.add("orgName", orgName);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/bind/department").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Updating Organization
    @PostMapping(value = "/update")
    public ResponseEntity<Boolean> updateOrganization(@RequestHeader String orgName,
            @RequestBody AppOrganization organization) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("orgName", orgName);
        RequestEntity<AppOrganization> req = RequestEntity.post(baseUrl + "/update").headers(headers).body(organization);
        return template.exchange(req, Boolean.class);
    }

    // $ Deleting Organization
    @DeleteMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteOrganization(@RequestHeader String orgName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("orgName", orgName);
        RequestEntity<Void> req = RequestEntity.delete(baseUrl + "/delete").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }

    // $ Unbinding entities
    @PostMapping(value = "/unbind/department")
    public ResponseEntity<Boolean> unbindDepartmentFromOrganization(@RequestHeader String depName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("depName", depName);
        RequestEntity<Void> req = RequestEntity.post(baseUrl + "/unbind/department").headers(headers).build();
        return template.exchange(req, Boolean.class);
    }
}
